"""
Utility program to get all devices on a T-Mobile NB-IoT account to a csv file, suitable for re-upload to a project.
Optionally the devices are optionally deleted from the source project.
Author: Gijs Mos, Matanga BV.
Copyright and MIT license: https://gijs-mosmatangabv.mit-license.org/
"""
import sys
from base64 import b64encode
from getpass import getpass
from typing import Optional

import requests
import typer

BASE_URL = "https://api.scs.iot.telekom.com/"

app = typer.Typer(add_completion=False)


def get_devices(username, password):

    auth = b"Basic " + b64encode(f"{username}:{password}".encode("utf-8"))
    device_resp = requests.get(
        url=BASE_URL + "rest/device?iDisplayLength=-1",
        headers={
            "Authorization": auth,
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
    )
    if device_resp.status_code != 200:
        typer.echo(f"Cannot get device list for user '{username}'", err=True)
        typer.echo(
            f"Error message: {device_resp.content.decode('utf-8')}.\nProgram aborted."
        )
        sys.exit(1)

    device_list = [
        (dd["id"], dd["deviceId"], dd["fullDeviceId"])
        for dd in device_resp.json().get("aaData", [])
    ]
    device_list.sort(key=lambda x: x[1])
    return device_list


def write_csv(device_list, output_file_name):
    def p(device_list, f):
        print("imei,protocol,username,password", file=f)
        for dev in device_list:
            print(f"{dev[1]},UDP,,", file=f)

    if output_file_name is None:
        p(device_list, sys.stdout)
    else:
        with open(output_file_name, "w") as f:
            p(device_list, f)


def purge_devices(device_list, username, password):
    def purge_one(dev_id, full_dev_id, auth):
        device_resp = requests.delete(
            url=f"{BASE_URL}rest/device/{dev_id}",
            headers={
                "Authorization": auth,
                "Content-Type": "application/json",
                "Accept": "application/json",
            },
        )
        if device_resp.status_code != 204:
            typer.echo(
                f"Cannot delete device {full_dev_id} list for user '{username}'",
                err=True,
            )
            typer.echo(
                f"Error message: {device_resp.content.decode('utf-8')}."
                "\nProgram aborted."
            )
            sys.exit(1)
        else:
            typer.echo(f"Deleted {full_dev_id}.", err=True)

    auth = b"Basic " + b64encode(f"{username}:{password}".encode("utf-8"))
    for dev in device_list:
        purge_one(dev[0], dev[2], auth)


@app.command()
def main(
    username: str,
    password: Optional[str] = typer.Argument(None),
    purge: bool = typer.Option(False, "-p", help="purge saved entries."),
    output_file_name: Optional[str] = typer.Option(
        None, "-o", help="Output file name. [default: stdout]"
    ),
):

    if password is None:
        password = getpass()

    dev_list = get_devices(username, password)
    write_csv(dev_list, output_file_name)

    if purge:
        purge_devices(dev_list, username, password)


if __name__ == "__main__":
    app()
